package com.latentsoft.partharaj.trainthebrain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class firstpage extends AppCompatActivity {

    Button btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstpage);
       // btn1= (Button) this.<View>findViewById(R.id.button);
        btn1 = findViewById(R.id.btn_first_page);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent= new Intent(firstpage.this,OptionActivity.class);
                startActivity(intent);
            }
        });

    }
}
