package com.latentsoft.partharaj.trainthebrain;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class PuzzlerAdapter_medium extends RecyclerView.Adapter<PuzzlerAdapter_medium.PuzzleViewHolder> {

    OnAnswerGiven onAnswerGiven;
    Context context;
    Puzzler_medium puzzler;
    ArrayList<String> colorCodes = new ArrayList<>();

    public PuzzlerAdapter_medium(Context context, Puzzler_medium puzzler, ArrayList<String> colorCodes) {
        this.context = context;
        this.puzzler = puzzler;
        this.colorCodes = colorCodes;
    }

    public void setOnAnswerGivenListener(OnAnswerGiven onAnswerGiven){
        this.onAnswerGiven = onAnswerGiven;
    }

    @Override
    public PuzzleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_color_card, parent, false);
        return new PuzzleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PuzzleViewHolder holder, int position) {
//        holder.iv.setBackgroundColor(Color.parseColor(colorCodes.get(position)));
        holder.iv.setText(colorCodes.get(position));

    }

    @Override
    public int getItemCount() {
        return colorCodes.size();
    }


    class PuzzleViewHolder extends RecyclerView.ViewHolder{

        TextView iv;

        public PuzzleViewHolder(View itemView) {
            super(itemView);

            iv = itemView.findViewById(R.id.iv_color);

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String selectedColor = colorCodes.get(getAdapterPosition());
                    if (selectedColor.equals(puzzler.getCorrectColorCode())) {
//                        Toast.makeText(context, "Accept!", Toast.LENGTH_SHORT).show();
//                        Log.e("PD_DEBUG", "Accept.");
                        onAnswerGiven.onCorrect();
                    }else{
//                        Toast.makeText(context, "Wrong Answer!", Toast.LENGTH_SHORT).show();
//                        Log.e("PD_DEBUG", "Wrong Answer.");
                        onAnswerGiven.onWrong();
                    }
                }
            });
        }
    }

    public interface OnAnswerGiven{
        void onCorrect();
        void onWrong();
    }
}
