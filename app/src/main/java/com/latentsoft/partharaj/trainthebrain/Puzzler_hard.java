package com.latentsoft.partharaj.trainthebrain;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

public class Puzzler_hard {

    ArrayList<String> colorString = new ArrayList<>();
    ArrayList<String> colorCodes = new ArrayList<>();

    ArrayList<String> pickedColorCodes = new ArrayList<>();

    private String correctColorCode;

    private int pickedPosition;

    public Puzzler_hard() {
        init();
    }

    public void generate(){
        pickedPosition = (int) (Math.floor(Math.abs(Math.random()*10)) % colorString.size());
        Log.e("PD_DEBUG", "Picked Position : "+pickedPosition);
    }

    public String getColorString(){
        return colorString.get(pickedPosition);
    }

    public ArrayList<String> getColorCodes(){
        pickedColorCodes.add(colorCodes.get(pickedPosition));
        correctColorCode = colorCodes.get(pickedPosition);

        ArrayList<String> tempColors = colorCodes;
        tempColors.remove(pickedPosition);
        Collections.shuffle(tempColors);

        for (int i= 0; i< 3; i++) {
            pickedColorCodes.add(tempColors.get(i));
        }

        Collections.shuffle(pickedColorCodes);

        return pickedColorCodes;
    }

    public String getCorrectColorCode(){
        return correctColorCode;
    }

    public int getPickedPosition() {
        return pickedPosition;
    }

    private void init() {
        colorString.add("(3 * 6) / 2 = ?");
        colorString.add("(4 + 6 - 4) = ?");
        colorString.add("(3 - 9) * 3 = ?");
        colorString.add("(10 / 5 * 2) = ?");
        colorString.add("4 * 7 - 8 = ?");
        colorString.add("16 / 4 + 2 = ?");
        colorString.add("6 * 6 -6 = ?");
        colorString.add("15 / 3 + 5 = ?");
        colorString.add("12 / 4 + 3 = ?");

        colorCodes.add("Ans = 9");
        colorCodes.add("Ans = 6");
        colorCodes.add("Ans = -18");
        colorCodes.add("Ans = 4");
        colorCodes.add("Ans = 20");
        colorCodes.add("Ans = 6");
        colorCodes.add("Ans = 30");
        colorCodes.add("Ans = 10");
        colorCodes.add("Ans = 6");
    }
}
